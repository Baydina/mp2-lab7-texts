#pragma once

#include "tdatacom.h"
#include "ttextlink.h"
#include <stack>
#include <string>
#include <fstream>

class TText;
typedef TText *PTText;

class TText : public TDataCom {
protected:
  PTTextLink pFirst;                                    // ��������� ����� ������
  PTTextLink pCurrent;                                  // ��������� ������� ������
  stack< PTTextLink > Path;                             // ���� ���������� �������� �� ������
  stack< PTTextLink > St;                               // ���� ��� ���������
  PTTextLink GetFirstAtom (PTTextLink pl);              // ����� ������� �����
  void PrintText (PTTextLink ptl);                      // ������ ������ �� ����� ptl
  void PrintTextFile(PTTextLink ptl, ofstream& TxtFile);
  PTTextLink ReadText(ifstream& TxtFile);               // ������ ������ �� �����
  static int TextLevel;
public:
  TText (PTTextLink pl = nullptr);
  ~TText () {pFirst = nullptr;}
// ���������
  Data GoFirstLink (void);            // ������� � ������ ������
  Data GoDownLink (void);             // ������� � ��������� ������ �� Down
  Data GoNextLink (void);             // ������� � ��������� ������ �� Next
  Data GoPrevLink (void);             // ������� � ���������� ������� � ������
// ������
  string GetLine(void);               // ������ ������� ������
  void SetLine ( string str);         // ������ ������� ������ 
// �����������
  void InsDownLine (string str);      // ������� ������ � ����������
  void InsDownSection (string str);   // ������� ������� � ����������
  void InsNextLine (string str);      // ������� ������ � ��� �� ������
  void InsNextSection (string str);   // ������� ������� � ��� �� ������
  void DelDownLine (void);            // �������� ������ � ���������
  void DelDownSection (void);         // �������� ������� � ���������
  void DelNextLine (void);            // �������� ������ � ��� �� ������
  void DelNextSection (void);         // �������� ������� � ��� �� ������
// ��������
  Data Reset (void);                  // ���������� �� ������ �������
  bool IsTextEnded (void) const;      // ����� ��������?
  bool GoNext (void);                 // ������� � ��������� ������
//������ � �������
  void Read (char* pFileName);        // ���� ������ �� �����
  void Write (char* pFileName);       // ����� ������ � ����
//������
  void Print (void);                  // ������ ������
};
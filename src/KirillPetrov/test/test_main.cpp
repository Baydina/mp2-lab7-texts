// Copyright 2016 Petrov Kirill

#define TEST

#ifdef TEST

#include <gtest.h>

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
#else 

// SAMPLE
#include "ttext.h"

int main()
{
	TTextLink A;
}

#endif
set(target "tests")

file(GLOB srcs "*.cpp")

find_package(Boost REQUIRED)
if (NOT Boost_FOUND)
	message (SEND_ERROR "Failed to find boost!")
	return()
else()
	include_directories(${Boost_INCLUDE_DIRS})
endif()

#add_library(library STATIC ${SOURCE_LIB})
add_executable(${target} ${srcs} ${hdrs})

target_link_libraries(${target} src ${LIBRARY})
target_link_libraries(${target} gtest ${LIBRARY})
target_link_libraries(${target} ${Boost_LIBRARIES})